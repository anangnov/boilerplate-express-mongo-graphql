const { gql } = require('apollo-server-express');

const typeDefs = gql`
  type Content {
    id: ID!
    title: String!
    description: String
  }

  type Query {
    contents: [Content]
    content(id: ID!): Content
  }

  type Mutation {
    createContent(title: String!, description: String): Content
    updateContent(id: ID!, title: String, description: String): Content
    deleteContent(id: ID!): Content
  }
`;

module.exports = typeDefs;
