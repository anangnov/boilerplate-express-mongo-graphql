const express = require('express');
const { ApolloServer } = require('apollo-server-express');
const mongoose = require('mongoose');
const typeDefs = require('./schema');
const resolvers = require('./resolvers');

const app = express();

// connection to mongodb
mongoose.connect('mongodb://localhost:27017/app-graphql-test');
mongoose.connection.once('open', () => {
    console.log('Connected to MongoDB');
});

// definition and call apollo server
const server = new ApolloServer({
    typeDefs,
    resolvers
});

async function startApolloServer() {
    await server.start();

    server.applyMiddleware({ app: app });

    const PORT = 3000;

    app.listen(PORT, () => {
        console.log(`Server listening on http://localhost:${PORT}${server.graphqlPath}`);
    });
}

startApolloServer();