const Content = require('./models/content');

const resolvers = {
    // Get All & Detail
    Query: {
        contents: async () => {
            try {
                return await Content.find();
            } catch (e) {
                throw new Error('Error');
            }
        },
        content: async (parent, { id }) => {
            try {
                return await Content.findById(id);
            } catch (e) {
                throw new Error('Error');
            }
        }
    },
    // Create, Update & Delete
    Mutation: {
        createContent: async (parent, { title, description }) => {
            try {
                const content = new Content({ title, description });

                return await content.save();
            } catch (e) {
                throw new Error('Error');
            }
        },
        updateContent: async (parent, { id, title, description }) => {
            try {
                return await Content.findByIdAndUpdate(id, { title, description }, { new: true });
            } catch (e) {
                throw new Error('Error');
            }
        },
        deleteContent: async (parent, { id }) => {
            try {
                return await Content.findByIdAndDelete(id);
            } catch (e) {
                throw new Error('Error');
            }
        }
    }
}

module.exports = resolvers;
